package Service.AddNota;

import Service.Base.TestServiceMain;
import domain.Nota;
import domain.Student;
import domain.Tema;
import junit.framework.TestCase;
import org.junit.Test;

import java.time.LocalDate;

import static junit.framework.TestCase.assertTrue;
import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;

public class TestAddNota extends TestServiceMain {
    @Test
    public void testAddStudent() {
        this.service.deleteStudent("202");
        Long sizeInitial = this.service.getAllStudenti().spliterator().getExactSizeIfKnown();
        Student student = new Student("202", "Denis", 123, "fovasdenis@gmail.com");
        this.service.addStudent(student);
        Long sizeAfterSave = this.service.getAllStudenti().spliterator().getExactSizeIfKnown();
        assertEquals(1, sizeAfterSave - sizeInitial);
    }

    @Test
    public void testAddAssignment() {
        this.service.deleteTema("8");
        Long initialSize = this.service.getAllTeme().spliterator().getExactSizeIfKnown();
        Tema tema = new Tema("8", "Description", 10, 12);
        try {
            this.service.addTema(tema);
            assertTrue(true);
        } catch (NullPointerException e) {
            fail();
        }
        Long sizeAfterSaving = this.service.getAllTeme().spliterator().getExactSizeIfKnown();
        TestCase.assertEquals(1, (sizeAfterSaving - initialSize));
    }

    @Test
    public void testAddNota() {
        this.service.deleteNota("1");
        Student student = new Student("202", "Denis", 123, "fovasdenis@gmail.com");


        Tema tema = new Tema("8", "Description", 10, 12);

        this.service.addTema(tema);
        this.service.addStudent(student);
        Nota nota = new Nota("1", "202", "8", 8.0, LocalDate.of(2018, 12, 14));
        Long sizeBeforeSaving = this.service.getAllNote().spliterator().getExactSizeIfKnown();
        this.service.addNota(nota, "Good job, boyyy");
        Long sizeAfterSaving = this.service.getAllNote().spliterator().getExactSizeIfKnown();
        TestCase.assertEquals(1, (sizeAfterSaving - sizeBeforeSaving));


        this.service.deleteStudent("202");
        this.service.deleteTema("8");
    }

    @Test
    public void testAll() {
        this.service.deleteStudent("202");
        this.service.deleteTema("8");
        this.service.deleteNota("1");

        Long sizeInitial = this.service.getAllStudenti().spliterator().getExactSizeIfKnown();
        Student student = new Student("202", "Denis", 123, "fovasdenis@gmail.com");

        this.service.addStudent(student);
        Long sizeAfterSave = this.service.getAllStudenti().spliterator().getExactSizeIfKnown();

        assertEquals(1, sizeAfterSave - sizeInitial);

        Long initialSize = this.service.getAllTeme().spliterator().getExactSizeIfKnown();
        Tema tema = new Tema("8", "Description", 10, 12);
        try {
            this.service.addTema(tema);
            assertTrue(true);
        } catch (NullPointerException e) {
            fail();

        }
        Long sizeAfterSaving = this.service.getAllTeme().spliterator().getExactSizeIfKnown();
        TestCase.assertEquals(1, (sizeAfterSaving - initialSize));

        Nota nota = new Nota("1", "202", "8", 8.0, LocalDate.of(2018, 12, 14));
        Long sizeBeforeSaving = this.service.getAllNote().spliterator().getExactSizeIfKnown();
        this.service.addNota(nota, "Good job, boyyy");

        Long sizeAfterNotaSave = this.service.getAllNote().spliterator().getExactSizeIfKnown();
        TestCase.assertEquals(1, (sizeAfterNotaSave- sizeBeforeSaving));

        this.service.deleteStudent("202");
        this.service.deleteTema("8");
        this.service.deleteNota("1");
    }
}
