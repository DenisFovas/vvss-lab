package Service.addTema;

import Service.Base.TestServiceMain;
import domain.Tema;
import org.junit.Test;
import validation.ValidationException;

import static junit.framework.TestCase.*;

public class TestAddTema extends TestServiceMain {
    @Test
    public void shouldPass() {
        Tema tema = new Tema("121", "123", 1, 2);
        Long initialSize = this.service.getAllTeme().spliterator().getExactSizeIfKnown();
        this.service.addTema(tema);
        Long sizeAfterSaving = this.service.getAllTeme().spliterator().getExactSizeIfKnown();
        assertEquals(1, (sizeAfterSaving - initialSize));
        this.service.deleteTema("121");
        // Now should be 0, duuh
        sizeAfterSaving = this.service.getAllTeme().spliterator().getExactSizeIfKnown();
        assertEquals(0, (sizeAfterSaving - initialSize));
    }

    @Test
    public void dontSaveIfNull() {
        Long initialSize = this.service.getAllTeme().spliterator().getExactSizeIfKnown();
        try {
            Tema tema = new Tema("8", "Description", 10, 12);
            this.service.addTema(tema);
            assertTrue(true);
        } catch (NullPointerException e) {
            fail();
        }
    }

    @Test
    public void dontSaveIfEmptyID() {
        Tema tema = new Tema("", "Description", 1, 1);
        try {
            this.service.addTema(tema);
            fail();
        } catch (ValidationException e) {
            assertTrue(true);
        }
    }

    @Test
    public void dontSaveIfEmptyDescription() {
        Tema tema = new Tema("123", "", 1, 1);
        invalidTemaShouldFail(tema);
    }


    @Test
    public void dontSaveIfInvalidDeadline() {
        Tema tema = new Tema("123", "Description", 100, 2);
        invalidTemaShouldFail(tema);
    }

    @Test
    public void dontSaveIfDeadlineIntervalInvalid() {
        Tema tema = new Tema("123", "Desc", 13, 2);
        invalidTemaShouldFail(tema);
    }

    @Test
    public void dontSaveIfReceivedInvalid() {
        Tema tema = new Tema("123", "", 13, 232);
        this.invalidTemaShouldFail(tema);
    }

    private void invalidTemaShouldFail(Tema tema) {
        try {
            this.service.addTema(tema);
            this.service.deleteTema("123");
            fail();
        } catch (ValidationException e) {
            assertTrue(true);
        }
    }
}
