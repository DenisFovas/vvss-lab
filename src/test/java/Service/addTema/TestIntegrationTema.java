package Service.addTema;

import Service.Base.TestServiceMain;
import domain.Nota;
import domain.Student;
import domain.Tema;
import junit.framework.TestCase;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class TestIntegrationTema extends TestServiceMain {

    @Test
    public void testAddStudent() {
        this.service.deleteStudent("200");

        Long sizeInitial = this.service.getAllStudenti().spliterator().getExactSizeIfKnown();
        Student student = new Student("200", "Denis", 123, "fovasdenis@gmail.com");
        this.service.addStudent(student);


        Long sizeAfterSave = this.service.getAllStudenti().spliterator().getExactSizeIfKnown();
        assertEquals(1, sizeAfterSave - sizeInitial);
    }

    @Test
    public void testAddAssignment() {
        this.service.deleteStudent("250");
        this.service.deleteTema("200");

        Long sizeInitial = this.service.getAllStudenti().spliterator().getExactSizeIfKnown();
        Student student = new Student("250", "Denis", 123, "fovasdenis@gmail.com");
        this.service.addStudent(student);


        Long sizeAfterSave = this.service.getAllStudenti().spliterator().getExactSizeIfKnown();
        assertEquals(1, sizeAfterSave - sizeInitial);

        Long sizeInitialTema = this.service.getAllTeme().spliterator().getExactSizeIfKnown();

        Tema tema = new Tema("200", "Random tema", 3, 5);
        this.service.addTema(tema);

        Long sizeAfterSaveTema = this.service.getAllTeme().spliterator().getExactSizeIfKnown();
        assertEquals(1, sizeAfterSaveTema - sizeInitialTema);
        this.service.deleteStudent("250");
        this.service.deleteTema("200");
    }


    @Test
    public void testAddGrade() {
        this.service.deleteNota("1012");
        this.service.deleteTema("100");
        this.service.deleteStudent("277");

        Long sizeInitial = this.service.getAllStudenti().spliterator().getExactSizeIfKnown();
        Student student = new Student("277", "Denis", 123, "fovasdenis@gmail.com");
        this.service.addStudent(student);

        Long sizeAfterSave = this.service.getAllStudenti().spliterator().getExactSizeIfKnown();
        assertEquals(1, sizeAfterSave - sizeInitial);


        Long sizeInitialTema = this.service.getAllTeme().spliterator().getExactSizeIfKnown();

        Tema tema = new Tema("100", "Random tema", 13, 13);
        this.service.addTema(tema);

        Long sizeAfterSaveTema = this.service.getAllTeme().spliterator().getExactSizeIfKnown();
        assertEquals(1, sizeAfterSaveTema - sizeInitialTema);

        Nota nota = new Nota("1012", "277", "100", 8.0, LocalDate.of(2018, 12, 29));
        Long sizeBeforeSavingGrade = this.service.getAllNote().spliterator().getExactSizeIfKnown();
        this.service.addNota(nota, "Good job, boyyy");
        Long sizeAfterSavingGrade = this.service.getAllNote().spliterator().getExactSizeIfKnown();
        TestCase.assertEquals(1, (sizeAfterSavingGrade - sizeBeforeSavingGrade));

        this.service.deleteNota("1012");
        this.service.deleteTema("100");
        this.service.deleteStudent("277");

    }
}
