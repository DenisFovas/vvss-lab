package Service.AddStudent;

import Service.Base.TestServiceMain;
import domain.Student;
import org.junit.Test;
import validation.ValidationException;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TestAddStudent extends TestServiceMain {

    @Test
    public void shouldPass() {
        Long sizeInitial = this.service.getAllStudenti().spliterator().getExactSizeIfKnown();
        Student student = new Student("213213", "223", 9999, "123@adsad.asda");

        this.service.addStudent(student);
        Long size = this.service.getAllStudenti().spliterator().getExactSizeIfKnown();

        assertEquals(1, size - sizeInitial);

        this.service.deleteStudent("213213");
        size = this.service.getAllStudenti().spliterator().getExactSizeIfKnown();
        assertEquals(0, size - sizeInitial);
    }

    @Test
    public void checkIdEmpty() {
        String id = "";
        Student student = new Student(id, "223", 9999, "ad@a.com");
        try {
            this.service.addStudent(student);
            this.service.deleteStudent(id);
            fail("ERROR: Student shouldn't be saved with null id");
        } catch (ValidationException e) {
            assertTrue(true);
        }
    }

    @Test
    public void checkGroupGreaterThanZero() {
        int group = -2545;
        Student student = new Student("213213", "223", group, "asad@a.com");
        try {
            this.service.addStudent(student);
            this.service.deleteStudent("213213");
            fail("ERROR: Student shouldn't be saved with null id");
        } catch (ValidationException e) {
            assertTrue(true);
        }
    }

    @Test
    public void checkGroupValid() {
        int group = 2545;
        Student student = new Student("213213", "223", group, "asad@a.com");
        Long sizeInitial = this.service.getAllStudenti().spliterator().getExactSizeIfKnown();
        this.service.addStudent(student);
        Long sizeAfterSave = this.service.getAllStudenti().spliterator().getExactSizeIfKnown();
        assertEquals(1, sizeAfterSave - sizeInitial);
        this.service.deleteStudent("213213");
        sizeAfterSave = this.service.getAllStudenti().spliterator().getExactSizeIfKnown();
        assertEquals(0, sizeAfterSave - sizeInitial);
    }

    @Test
    public void checkEmailNull() {
        Student student = new Student("213213", "223", 9999, null);
        checkEmailAddress(student);
    }

    @Test
    public void checkEmailNotEmpty() {
        String email = "";
        Student student = new Student("213213", "223", 9999, email);
        checkEmailAddress(student);
    }

    @Test
    public void checkEmailNotValidFormat() {
        String email = "1231@a";
        Student student = new Student("213213", "223", 9999, email);
        checkEmailAddress(student);
    }

    @Test
    public void checkEmailValidFormat() {
        String email = "1231@a.com";
        Student student = new Student("213213", "223", 9999, email);
        Long sizeInitial = this.service.getAllStudenti().spliterator().getExactSizeIfKnown();
        this.service.addStudent(student);
        Long size = this.service.getAllStudenti().spliterator().getExactSizeIfKnown();
        assertEquals(1, size - sizeInitial);
        this.service.deleteStudent("213213");
        size = this.service.getAllStudenti().spliterator().getExactSizeIfKnown();
        assertEquals(0, size - sizeInitial);

    }

    private void checkEmailAddress(Student student) {
        try {
            this.service.addStudent(student);
            this.service.deleteStudent("213213");
            fail("ERROR: student with email " + student.getEmail() + " shouldn't be saved.");
        } catch (ValidationException e) {
            assertTrue(true);
        }
    }
}
